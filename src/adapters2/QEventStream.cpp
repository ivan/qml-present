/*
 *   Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "QEventStream.h"

#include <QGuiApplication>
#include <QDebug>

#include <vector>
#include <utils/d_ptr_implementation.h>

class QEventStream::Private {
public:
    std::vector<QEventStream::Continuation> continuations;

};

QEventStream::QEventStream(QObject *parent)
    : QObject(parent)
{
    parent->installEventFilter(this);
}

QEventStream::~QEventStream()
{
}

bool QEventStream::eventFilter(QObject * watched, QEvent * event)
{
    Q_UNUSED(watched);

    qDebug() << "EVENT!";

    for (const auto& continuation: d->continuations) {
        continuation(event);
    }

    return false;
}

void QEventStream::then(Continuation f)
{
    d->continuations.push_back(f);
}

QEventStream &make_qevent_stream(QObject *object)
{
    // This is dangerous :)
    return *(new QEventStream(object));
}

