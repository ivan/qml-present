/*
 *   Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef EVENTSTREAM_H
#define EVENTSTREAM_H

#include <QObject>
#include <functional>

#include <utils/d_ptr.h>

class QEvent;
class QObject;

class QEventStream: public QObject {
public:
    using Continuation = std::function<void(QEvent*)>;

    QEventStream(QObject *parent);
    ~QEventStream();

    bool eventFilter(QObject *watched, QEvent *event) Q_DECL_OVERRIDE;

    void then(Continuation f);

private:
    D_PTR;
};

QEventStream &make_qevent_stream(QObject *object);

#endif // EVENTSTREAM_H



