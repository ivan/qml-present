project (Skot)

# Extra CMake stuff
include (FeatureSummary)
find_package (ECM 5.11.0  NO_MODULE)
set (
   CMAKE_MODULE_PATH
   ${CMAKE_MODULE_PATH}
   ${ECM_MODULE_PATH}
   ${ECM_KDE_MODULE_DIR}
   ${CMAKE_SOURCE_DIR}/cmake/modules
   )

# Finding Qt
set (REQUIRED_QT_VERSION 5.4.0)
set (CMAKE_AUTOMOC ON)
find_package(Qt5 COMPONENTS Core Quick Xml)

# Some sane settings
include (KDEInstallDirs)
include (KDECMakeSettings)
include (KDECompilerSettings)
include (GenerateExportHeader)
include (ECMGenerateHeaders)

# Some insane settings
add_definitions("-std=c++14")

# Finding other deps
find_package(Poppler "0.12.1" REQUIRED)
set_package_properties("Poppler-Qt5" PROPERTIES
        DESCRIPTION "A PDF rendering library"
        URL "http://poppler.freedesktop.org"
        TYPE RECOMMENDED
        PURPOSE "Support for PDF.")

set (
   QT_PRESENT_SRCS

   main.cpp

   adapters/mouseadapter.cpp
   adapters/positioningitem.cpp

   adapters2/QEventStream.cpp

   components/pdf/PdfPage.cpp
   components/pdf/PdfDocument.cpp
   )

add_executable (
   skot
   ${QT_PRESENT_SRCS}
   )

target_link_libraries (
   skot
   Qt5::Core
   Qt5::Quick
   Qt5::Xml
   ${POPPLER_LIBRARY}
   )
