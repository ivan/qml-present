/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.4

import org.kde.slides 1.0

Item {
    id: root

    property variant document: PdfDocument {}

    Rectangle {
        anchors.fill: parent

        color: 'black'
        opacity: .8
    }

    Item {
        id: content

        property double itemSpacing : (content.width - 320) / root.document.pageCount

        anchors {
            fill: parent
            margins: 16
        }

        Repeater {
            model: root.document == null ? null : root.document.pageCount

            PdfPage {
                id: pdfPage

                document: root.document
                pageNumber : modelData

                width: 320
                height: 240

                property double baseX: modelData * content.itemSpacing

                x: baseX + (pdfPage.baseX + 64 > mouse.mouseX ? 100 : 0)
                y: content.height / 2

                property bool isCurrent:
                    (pdfPage.baseX + 64 < mouse.mouseX) &&
                    (pdfPage.baseX + content.itemSpacing + 64 > mouse.mouseX)

                Behavior on x { NumberAnimation { duration: 300; easing.type: Easing.OutSine } }

                transform: Rotation { axis { x: 0.3; y: 1; z: -0.5 } angle: 60 }

                Rectangle {
                    color: 'black'

                    anchors.fill: parent

                    opacity: pdfPage.isCurrent ? 0 : .5
                    Behavior on opacity { NumberAnimation { duration: 300; easing.type: Easing.OutSine } }
                }
            }
        }

        MouseArea {
            id: mouse
            anchors.fill: parent
            hoverEnabled: true
        }
    }
}

