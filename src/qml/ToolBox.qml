/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.4

Item {
    id: root

    signal slideAction(string action)

    property string selectedTool: 'none'

    onSelectedToolChanged: console.log(selectedTool)

    height: content.height + 2 * padding

    property int padding: 4

    MouseArea {
        anchors.fill : parent
        hoverEnabled : true
        onEntered    : root.state = 'shown'
    }

    Image {
        id: shadow
        source: "images/toolbox-shadow.png"

        anchors {
            verticalCenter: background.verticalCenter
            left: background.right
        }

        height  : background.height - 2 * root.padding
        width   : 50
        opacity : .3

        Behavior on opacity { NumberAnimation { duration: 300; easing.type: Easing.OutSine } }
    }

    Rectangle {
        id: background

        width  : parent.width
        height : root.height

        color  : '#31363b'
        radius : root.padding

        Rectangle {
            color: background.color

            width: root.padding

            anchors {
                left   : parent.left
                top    : parent.top
                bottom : parent.bottom
            }
        }

        Behavior on x { NumberAnimation { duration: 300; easing.type: Easing.OutSine } }

        Rectangle {
            id: selectedToolBackground

            color  : '#2980b9'
            width  : background.width
            height : background.width - root.padding
            y      : root.padding

            Behavior on y { NumberAnimation { duration: 300; easing.type: Easing.OutSine } }
        }

        Column {
            id: content

            width : parent.width - root.padding
            y     : root.padding

            Repeater {
                model: [
                    'Navigation',
                    'Spline',
                    'Freehand',
                    'Rectangle',
                    'Wide Rectangle',
                    '-',
                    'Eraser',
                    'Clear' ]

                Item {
                    id: item
                    property string tool : modelData

                    height: tool == '-' ? separator.height : button.height
                    width:  parent.width

                    ToolButton {
                        id: button

                        visible    : item.tool != '-'

                        iconSource : 'icons/tool: ' + item.tool + '.svg'
                        width      : parent.width
                        height     : parent.width

                        tooltip    : item.tool

                        onClicked  : root.selectedTool = item.tool

                        Connections {
                            target: root
                            onSelectedToolChanged: {
                                if (root.selectedTool == item.tool) {
                                    selectedToolBackground.y = y + root.padding;
                                    selectedToolBackground.height = height;
                                }
                            }
                        }
                    }

                    ToolBoxSeparator {
                        id: separator

                        visible    : item.tool == '-'
                    }
                }
            }

            ToolBoxSeparator {}

            ToolButton {
                property string action : "Change Slide"

                iconSource   : 'icons/slide: ' + action + '.svg'
                width        : parent.width
                height       : parent.width

                tooltip      : mouseAreaPreviousSlide.containsMouse ?
                               "Previous Slide" : "Next Slide"

                tooltipShown : mouseAreaNextSlide.containsMouse ||
                               mouseAreaPreviousSlide.containsMouse

                MouseArea {
                    id: mouseAreaPreviousSlide

                    hoverEnabled : true
                    onClicked    : root.slideAction("Previous Slide")
                    onWheel      : root.slideAction(
                                       wheel.angleDelta.y > 0 ?
                                           "Previous Slide" : "Next Slide")

                    anchors {
                        left   : parent.left
                        right  : parent.horizontalCenter
                        top    : parent.top
                        bottom : parent.bottom
                    }
                }

                MouseArea {
                    id: mouseAreaNextSlide

                    hoverEnabled : true
                    onClicked    : root.slideAction("Next Slide")
                    onWheel      : root.slideAction(
                                       wheel.angleDelta.y > 0 ?
                                           "Previous Slide" : "Next Slide")

                    anchors {
                        right  : parent.right
                        left   : parent.horizontalCenter
                        top    : parent.top
                        bottom : parent.bottom
                    }
                }
            }

            Repeater {
                model: [
                    'Go To',
                    'Overview'
                ]

                ToolButton {
                    property string action : modelData

                    tooltip    : action

                    iconSource : 'icons/slide: ' + action + '.svg'
                    width      : parent.width
                    height     : parent.width

                    onClicked  : root.slideAction(action)

                }
            }

        }
    }

    state: 'shown'

    states: [
        State {
            name: 'hidden'
            PropertyChanges { target: root; opacity: .3 }
            PropertyChanges { target: shadow; opacity: 0 }
            PropertyChanges { target: background; x: - content.width }
        },
        State {
            name: 'shown'
            PropertyChanges { target: root; opacity: 1 }
            PropertyChanges { target: shadow; opacity: 0.3 }
            PropertyChanges { target: background; x: 0 }
        }
    ]

    Behavior on opacity { NumberAnimation { duration: 300; easing.type: Easing.OutSine } }
}

