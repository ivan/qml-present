/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.4
import QtQuick.Controls 1.3

import org.kde.slides 1.0

ApplicationWindow {
    title   : qsTr("Hello World")
    width   : 1024
    height  : 768
    visible : true

    PdfDocument {
        id: pdfDocument

        filePath : "../presentation.pdf"
    }

    PdfPage {
        id: pdf

        document     : pdfDocument
        pageNumber   : 7
        anchors.fill : parent
    }

    PdfPageOverview {
        id: pageOverview

        anchors.fill : parent
        document     : pdfDocument
        visible      : false
    }

    ToolBox {
        id: toolbox

        width: 48

        anchors {
            left           : parent.left
            verticalCenter : parent.verticalCenter
        }

        onSlideAction: {
            if (action == 'Previous Slide') {
                pdf.pageNumber--;

            } else if (action == 'Next Slide') {
                pdf.pageNumber++;

            } else if (action == 'Overview') {
                pageOverview.visible = !pageOverview.visible

            }
        }

        state: 'hidden'
    }


    Rectangle { objectName: "mouseCursorPresentation" //_ }

        width: 32
        height: 32

        color: "transparent"
        border.color: "red"
        border.width: 2
        radius: 10

        property point center: Qt.point(0, 0)

        onCenterChanged: {
            x = center.x - width / 2;
            y = center.y - height / 2;
        }
    }
    //^

    Rectangle { objectName: "topRulerMarker" //_ }

        width: 16
        height: 16

        color: "green"
        border.color: "green"
        border.width: 2
        radius: 10

        property point center: Qt.point(0, 0)

        onCenterChanged: {
            x = center.x - width / 2;
            y = center.y - height / 2;
        }
    } //^

    Rectangle { objectName: "leftRulerMarker" //_ }

        width: 16
        height: 16

        color: "green"
        border.color: "green"
        border.width: 2
        radius: 10

        property point center: Qt.point(0, 0)

        onCenterChanged: {
            x = center.x - width / 2;
            y = center.y - height / 2;
        }
    } //^

    Rectangle { objectName: "filterMarker" //_ }

        width: 8
        height: 8

        color: "steelblue"
        border.color: "#334455"
        border.width: 2
        radius: 0

        property point center: Qt.point(0, 0)

        onCenterChanged: {
            x = center.x - width / 2;
            y = center.y - height / 2;
        }

        Rectangle {
            width: 2
            height: 10000
            z: -1

            color: "#334455"

            anchors.centerIn: parent
        }

        Rectangle {
            width: 10000
            height: 2
            z: -1

            color: "#334455"

            anchors.centerIn: parent
        }
    } //^

    Rectangle { objectName: "gravityMarker" //_ }

        width: 16
        height: 16

        color: "steelblue"
        radius: 16

        property point center: Qt.point(0, 0)

        onCenterChanged: {
            x = center.x - width / 2;
            y = center.y - height / 2;
        }
    } //^
}
