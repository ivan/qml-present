/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.4
import QtQuick.Controls 1.3

Item {
    id: root
    property alias iconSource: icon.source

    property alias tooltip: text.text

    property bool tooltipShown: mouse.containsMouse && root.tooltip != ""

    signal clicked()

    Image {
        id: icon
        anchors.centerIn: parent
    }

    MouseArea {
        id: mouse
        anchors.fill: parent
        onClicked: root.clicked()

        hoverEnabled: true
    }

    Item {
        visible: root.tooltipShown

        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.right
            leftMargin: 8
        }

        height : text.height + 16
        width  : text.width + 32

        Rectangle {
            color: background.color

            anchors {
                verticalCenter: parent.verticalCenter
            }

            width  : 8
            height : 8

            transform: Rotation { origin.x: 4; origin.y: 4; angle: 45 }
        }

        Rectangle {
            id: background

            color  : '#232629'

            width  : text.width + 16
            height : text.height + 16
            radius : 4

            x      : 4

            Text {
                id: text

                color : 'white'
                x     : 8
                y     : 8
            }
        }
    }
}

