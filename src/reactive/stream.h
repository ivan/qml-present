#ifndef STREAM_H
#define STREAM_H

namespace stream {

template <typename Stream, typename Cont>
auto operator >>= (Stream &&stream, Cont &&cont)
{
    return stream.then(cont);
}


template <typename Under>
class _stream {
public:
    _stream(Under under)
        : m_under(under)
    {
    }

    template <typename Cont>
    auto then(Cont &&cont) const
    {
        return m_under.then(cont);
    }

private:
    Under m_under;
};


template <typename Under>
auto make_stream(Under &&under)
{
    return _stream<Under>(under);
}

} // namespace stream

#include "fmap.h"
#include "tee.h"
#include "map.h"
#include "parallel.h"
#include "filter.h"

#endif // STREAM_H

