#ifndef STREAM_PARALLEL_H
#define STREAM_PARALLEL_H

namespace stream {

namespace detail {

    template <typename ... Conts>
    struct parallel_impl;

    template <typename Cont, typename ... Conts>
    struct parallel_impl<Cont, Conts...>: parallel_impl<Conts...>
    {
        using parent_type = parallel_impl<Conts...>;

        parallel_impl(Cont c, Conts... cs)
            : parent_type(cs...)
            , c(c)
        {
        }

        template <typename InType>
        void operator() (const InType &in) {
            c(in);
            parent_type::operator()(in);
        }

        Cont c;
    };

    template <typename Cont>
    struct parallel_impl<Cont>
    {
        parallel_impl(Cont c)
            : c(c)
        {
        }

        template <typename InType>
        void operator() (const InType &in) {
            c(in);
        }

        Cont c;
    };

} // namespace detail

template <typename ... Conts>
auto parallel(Conts ... cs) {
    return detail::parallel_impl<Conts...>(std::forward<Conts>(cs)...);
}

}

#endif // STREAM_PARALLEL_H

