#ifndef STREAM_FILTER_H
#define STREAM_FILTER_H

namespace stream {

namespace detail {

    template <typename Pred, typename Cont>
    struct filter_cont {
        filter_cont(Pred p, Cont c)
            : p(p)
            , c(c)
        {
        }

        template <typename InType>
        void operator () (const InType &in) {
            if (p(in)) {
                c(in);
            }
        }

        Pred p;
        Cont c;
    };

    template <typename Pred>
    struct filter_impl {
        filter_impl(Pred p)
            : p(p)
        {
        }

        template <typename Cont>
        auto then(Cont &&cont)
        {
            return filter_cont<Pred, Cont>(p, std::forward<Cont>(cont));
        }

        Pred p;
    };


} // namespace detail

template <typename Pred>
auto filter(Pred &&f)
{
    return detail::filter_impl<Pred>(std::forward<Pred>(f));
}

}






#endif // STREAM_FILTER_H

