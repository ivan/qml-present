#ifndef STREAMtee_impl_H
#define STREAMtee_impl_H

namespace stream {

namespace detail {

    template <typename Cont>
    struct tee_impl {
        tee_impl(Cont c)
            : c(c)
        {

        }

        Cont c;
    };

    template <typename Cont1, typename Cont2>
    struct tee_cont_impl {
        tee_cont_impl(tee_impl<Cont1> c1, Cont2 c2)
            : c1(c1)
            , c2(c2)
        {

        }

        template <typename InType>
        void operator() (const InType &in) {
            c1.c(in);
            c2(in);
        }

        tee_impl<Cont1> c1;
        Cont2 c2;
    };

    template <typename Cont1, typename Cont2>
    auto operator >>= (tee_impl<Cont1> && tee, Cont2 &&cont2) {
        return tee_cont_impl<Cont1, Cont2>(
                    std::forward<tee_impl<Cont1>>(tee),
                    std::forward<Cont2>(cont2));
    }

} // namespace detail

template <typename Cont>
auto tee(Cont &&c) {
    return detail::tee_impl<Cont>(std::forward<Cont>(c));
}

}

#endif // STREAMtee_impl_H

