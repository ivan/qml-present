#ifndef MOUSEADAPTER_H
#define MOUSEADAPTER_H

#include <QObject>
#include <QPointF>
#include <QQmlProperty>
#include <functional>

class MouseAdapter : public QObject
{
    Q_OBJECT
public:
    explicit MouseAdapter(QObject *parent = 0);

    template <typename T>
    void then(T &&cont) {
        m_continuation = cont;
    }

signals:
    void mousePositionChanged(QPointF position);

private slots:
    void onMousePositionChanged();

private:
    QQmlProperty m_mousePosition;

    // Qt can be hostile sometimes to lambdas, binds etc.
    std::function<void(QPointF)> m_continuation;
};

#endif // MOUSEADAPTER_H
