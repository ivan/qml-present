#include "positioningitem.h"

PositioningItem::PositioningItem(QObject *parent, QString objectName)
    : QObject(parent)
    , m_center(parent->findChild<QObject*>(objectName), "center")
{

}

void PositioningItem::moveTo(QPointF center)
{
    m_center.write(center);
}

