/*
 *  Copyright (C) 2011, 2014 Tino Kluge (ttk448 at gmail.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include <algorithm>

#include "Spline.h"

// BandMatrix implementation
// -------------------------

namespace {

// band matrix solver
class BandMatrix {
private:
    QVector<QVector<double>> m_upper; // upper band
    QVector<QVector<double>> m_lower; // lower band

public:
    BandMatrix()
    {
    };

    BandMatrix(int dim, int n_u, int n_l);

    ~BandMatrix()
    {
    };

    void resize(int dim, int n_u, int n_l); // init with dim,n_u,n_l
    int dim() const; // matrix dimension

    int num_upper() const
    {
        return m_upper.size() - 1;
    }

    int num_lower() const
    {
        return m_lower.size() - 1;
    }

    // access operator
    double &operator()(int i, int j); // write
    double operator()(int i, int j) const; // read

    // we can store an additional diogonal (in m_lower)
    double &saved_diag(int i);
    double saved_diag(int i) const;

    void lu_decompose();

    QVector<double> r_solve(const QVector<double> &b) const;
    QVector<double> l_solve(const QVector<double> &b) const;
    QVector<double> lu_solve(const QVector<double> &b,
                                 bool is_lu_decomposed = false);
};

BandMatrix::BandMatrix(int dim, int n_u, int n_l)
{
    resize(dim, n_u, n_l);
}

void BandMatrix::resize(int dim, int n_u, int n_l)
{
    Q_ASSERT(dim > 0);
    Q_ASSERT(n_u >= 0);
    Q_ASSERT(n_l >= 0);
    m_upper.resize(n_u + 1);
    m_lower.resize(n_l + 1);
    for (size_t i = 0; i < m_upper.size(); i++) {
        m_upper[i].resize(dim);
    }
    for (size_t i = 0; i < m_lower.size(); i++) {
        m_lower[i].resize(dim);
    }
}
int BandMatrix::dim() const
{
    if (m_upper.size() > 0) {
        return m_upper[0].size();
    }
    else {
        return 0;
    }
}

// defines the new operator (), so that we can access the elements
// by A(i,j), index going from i=0,...,dim()-1
double &BandMatrix::operator()(int i, int j)
{
    int k = j - i; // what band is the entry
    Q_ASSERT((i >= 0) && (i < dim()) && (j >= 0) && (j < dim()));
    Q_ASSERT((-num_lower() <= k) && (k <= num_upper()));
    // k=0 -> diogonal, k<0 lower left part, k>0 upper right part
    if (k >= 0)
        return m_upper[k][i];
    else
        return m_lower[-k][i];
}
double BandMatrix::operator()(int i, int j) const
{
    int k = j - i; // what band is the entry
    Q_ASSERT((i >= 0) && (i < dim()) && (j >= 0) && (j < dim()));
    Q_ASSERT((-num_lower() <= k) && (k <= num_upper()));
    // k=0 -> diogonal, k<0 lower left part, k>0 upper right part
    if (k >= 0)
        return m_upper[k][i];
    else
        return m_lower[-k][i];
}
// second diag (used in LU decomposition), saved in m_lower
double BandMatrix::saved_diag(int i) const
{
    Q_ASSERT((i >= 0) && (i < dim()));
    return m_lower[0][i];
}
double &BandMatrix::saved_diag(int i)
{
    Q_ASSERT((i >= 0) && (i < dim()));
    return m_lower[0][i];
}

// LR-Decomposition of a band matrix
void BandMatrix::lu_decompose()
{
    int i_max, j_max;
    int j_min;
    double x;

    // preconditioning
    // normalize column i so that a_ii=1
    for (int i = 0; i < this->dim(); i++) {
        Q_ASSERT(this->operator()(i, i) != 0.0);
        this->saved_diag(i) = 1.0 / this->operator()(i, i);
        j_min = std::max(0, i - this->num_lower());
        j_max = std::min(this->dim() - 1, i + this->num_upper());
        for (int j = j_min; j <= j_max; j++) {
            this->operator()(i, j) *= this->saved_diag(i);
        }
        this->operator()(i, i) = 1.0; // prevents rounding errors
    }

    // Gauss LR-Decomposition
    for (int k = 0; k < this->dim(); k++) {
        i_max = std::min(this->dim() - 1,
                         k + this->num_lower()); // num_lower not a mistake!
        for (int i = k + 1; i <= i_max; i++) {
            Q_ASSERT(this->operator()(k, k) != 0.0);
            x = -this->operator()(i, k) / this->operator()(k, k);
            this->operator()(i, k) = -x; // assembly part of L
            j_max = std::min(this->dim() - 1, k + this->num_upper());
            for (int j = k + 1; j <= j_max; j++) {
                // assembly part of R
                this->operator()(i, j) = this->operator()(i, j)
                                         + x * this->operator()(k, j);
            }
        }
    }
}
// solves Ly=b
QVector<double> BandMatrix::l_solve(const QVector<double> &b) const
{
    Q_ASSERT(this->dim() == (int)b.size());
    QVector<double> x(this->dim());
    int j_start;
    double sum;
    for (int i = 0; i < this->dim(); i++) {
        sum = 0;
        j_start = std::max(0, i - this->num_lower());
        for (int j = j_start; j < i; j++)
            sum += this->operator()(i, j) * x[j];
        x[i] = (b[i] * this->saved_diag(i)) - sum;
    }
    return x;
}
// solves Rx=y
QVector<double> BandMatrix::r_solve(const QVector<double> &b) const
{
    Q_ASSERT(this->dim() == (int)b.size());
    QVector<double> x(this->dim());
    int j_stop;
    double sum;
    for (int i = this->dim() - 1; i >= 0; i--) {
        sum = 0;
        j_stop = std::min(this->dim() - 1, i + this->num_upper());
        for (int j = i + 1; j <= j_stop; j++)
            sum += this->operator()(i, j) * x[j];
        x[i] = (b[i] - sum) / this->operator()(i, i);
    }
    return x;
}

QVector<double> BandMatrix::lu_solve(const QVector<double> &b,
                                         bool is_lu_decomposed)
{
    Q_ASSERT(this->dim() == (int)b.size());
    QVector<double> x, y;
    if (is_lu_decomposed == false) {
        this->lu_decompose();
    }
    y = this->l_solve(b);
    x = this->r_solve(y);
    return x;
}

}

// Spline implementation
// -----------------------

Spline::Spline(QObject *parent)
    : QObject(parent)
    , m_left(FirstDerivation)
    , m_right(FirstDerivation)
    , m_left_value(0.0)
    , m_right_value(0.0)
    , m_force_linear_extrapolation(false)
    , m_valid(false)
    , m_maximumY(1)
{
    qDebug() << "Spline::Spline";
}

// void Spline::setBoundary(Spline::Type left, double left_value,
//                          Spline::Type right, double right_value,
//                          bool force_linear_extrapolation)
// {
//     Q_ASSERT(m_xs.size() == 0); // set_points() must not have happened yet
//     m_left = left;
//     m_right = right;
//     m_left_value = left_value;
//     m_right_value = right_value;
//     m_force_linear_extrapolation = force_linear_extrapolation;
// }

void Spline::setXs(const QList< double >& xs)
{
    qDebug() << "Spline::setXs" << xs;
    m_valid = false;
    m_xs = xs;
    emit xsChanged(xs);
}

void Spline::setYs(const QList< double >& ys)
{
    qDebug() << "Spline::setYs" << ys;
    m_valid = false;
    m_ys = ys;
    emit ysChanged(ys);
}

const QList< double >& Spline::xs() const
{
    return m_xs;
}

const QList< double >& Spline::ys() const
{
    return m_ys;
}

QColor Spline::color() const
{
    return m_color;
}

void Spline::setColor(const QColor &color)
{
    m_color = color;
    emit colorChanged(color);
}

void Spline::setMaximumY(double maximumY)
{
    m_maximumY = maximumY;
    emit maximumYChanged(maximumY);
}

double Spline::maximumY() const
{
    return m_maximumY;
}


void Spline::calculate() const
{
    Q_ASSERT(m_xs.size() == m_ys.size());
    Q_ASSERT(m_xs.size() > 2);

    const int n = m_xs.size();

    // TODO: maybe sort x and y, rather than returning an error
    Q_ASSERT(std::is_sorted(m_xs.cbegin(), m_xs.cend()));

    // cubic Spline interpolation
    // setting up the matrix and right hand side of the equation system
    // for the parameters b[]
    BandMatrix A(n, 1, 1);
    QVector<double> rhs(n);

    for (int i = 1; i < n - 1; i++) {
        A(i, i - 1) = 1.0 / 3.0 * (m_xs[i] - m_xs[i - 1]);
        A(i, i) = 2.0 / 3.0 * (m_xs[i + 1] - m_xs[i - 1]);
        A(i, i + 1) = 1.0 / 3.0 * (m_xs[i + 1] - m_xs[i]);
        rhs[i] = (m_ys[i + 1] - m_ys[i]) / (m_xs[i + 1] - m_xs[i])
                    - (m_ys[i] - m_ys[i - 1]) / (m_xs[i] - m_xs[i - 1]);
    }

    // boundary conditions
    if (m_left == Spline::SecondDerivation) {
        // 2*b[0] = f''
        A(0, 0) = 2.0;
        A(0, 1) = 0.0;
        rhs[0] = m_left_value;

    } else if (m_left == Spline::FirstDerivation) {
        // c[0] = f', needs to be re-expressed in terms of b:
        // (2b[0]+b[1])(m_xs[1]-m_xs[0]) = 3 ((m_ys[1]-m_ys[0])/(m_xs[1]-m_xs[0]) - f')
        A(0, 0) = 2.0 * (m_xs[1] - m_xs[0]);
        A(0, 1) = 1.0 * (m_xs[1] - m_xs[0]);
        rhs[0] = 3.0 * ((m_ys[1] - m_ys[0]) / (m_xs[1] - m_xs[0]) - m_left_value);

    } else {
        Q_ASSERT(false);
    }

    if (m_right == Spline::SecondDerivation) {
        // 2*b[n-1] = f''
        A(n - 1, n - 1) = 2.0;
        A(n - 1, n - 2) = 0.0;
        rhs[n - 1] = m_right_value;

    } else if (m_right == Spline::FirstDerivation) {
        // c[n-1] = f', needs to be re-expressed in terms of b:
        // (b[n-2]+2b[n-1])(m_xs[n-1]-m_xs[n-2])
        // = 3 (f' - (m_ys[n-1]-m_ys[n-2])/(m_xs[n-1]-m_xs[n-2]))
        A(n - 1, n - 1) = 2.0 * (m_xs[n - 1] - m_xs[n - 2]);
        A(n - 1, n - 2) = 1.0 * (m_xs[n - 1] - m_xs[n - 2]);
        rhs[n - 1] = 3.0
                        * (m_right_value
                        - (m_ys[n - 1] - m_ys[n - 2]) / (m_xs[n - 1] - m_xs[n - 2]));

    }
    else {
        Q_ASSERT(false);
    }

    // solve the equation system to obtain the parameters b[]
    m_b = A.lu_solve(rhs);

    // calculate parameters a[] and c[] based on b[]
    m_a.resize(n);
    m_c.resize(n);

    for (int i = 0; i < n - 1; i++) {
        m_a[i] = 1.0 / 3.0 * (m_b[i + 1] - m_b[i]) / (m_xs[i + 1] - m_xs[i]);
        m_c[i] = (m_ys[i + 1] - m_ys[i]) / (m_xs[i + 1] - m_xs[i])
                    - 1.0 / 3.0 * (2.0 * m_b[i] + m_b[i + 1])
                        * (m_xs[i + 1] - m_xs[i]);
    }

    // for left extrapolation coefficients
    m_b0 = (m_force_linear_extrapolation == false) ? m_b[0] : 0.0;
    m_c0 = m_c[0];

    // for the right extrapolation coefficients
    // f_{n-1}(m_xs) = b*(m_xs-x_{n-1})^2 + c*(m_xs-x_{n-1}) + y_{n-1}
    double h = m_xs[n - 1] - m_xs[n - 2];
    // m_b[n-1] is determined by the boundary condition
    m_a[n - 1] = 0.0;
    m_c[n - 1] = 3.0 * m_a[n - 2] * h * h + 2.0 * m_b[n - 2] * h
                 + m_c[n - 2]; // = f'_{n-2}(x_{n-1})
    if (m_force_linear_extrapolation == true)
        m_b[n - 1] = 0.0;

    m_valid = true;
}

double Spline::operator() (double x) const
{
    if (!m_valid) calculate();
    if (!m_valid) return 0;

    size_t n = m_xs.size();
    // find the closest point m_xs[idx] < x, idx=0 even if x<m_xs[0]
    const auto it = std::lower_bound(m_xs.begin(), m_xs.end(), x);
    int idx = std::max(int(it - m_xs.begin()) - 1, 0);

    double h = x - m_xs[idx];
    double interpol;
    if (x < m_xs[0]) {
        // extrapolation to the left
        interpol = (m_b0 * h + m_c0) * h + m_ys[0];

    } else if (x > m_xs[n - 1]) {
        // extrapolation to the right
        interpol = (m_b[n - 1] * h + m_c[n - 1]) * h + m_ys[n - 1];

    } else {
        // interpolation
        interpol = ((m_a[idx] * h + m_b[idx]) * h + m_c[idx]) * h + m_ys[idx];
    }

    // qDebug() << "Spline(" << x << ") = " << interpol;

    return interpol;
}

