/*
 * Copyright (C) 2015  Ivan Čukić <ivan.cukic@kde.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "SplineChunk.h"

#include <QSGNode>
#include <QSGFlatColorMaterial>

#include "Spline.h"

SplineChunk::SplineChunk(QQuickItem *parent)
    : QQuickItem(parent)
    , m_segmentCount(32)
    , m_paintMode("fill")
    , m_maximumY(0)
{
    setFlag(ItemHasContents, true);
}

SplineChunk::~SplineChunk()
{

}

void SplineChunk::setSpline(Spline* spline)
{
    m_spline = spline;
    emit splineChanged(spline);
    update();
}

void SplineChunk::setRangeFrom(double from)
{
    m_range.first = from;
    emit rangeFromChanged(from);
    update();
}

void SplineChunk::setRangeTo(double to)
{
    m_range.second = to;
    emit rangeToChanged(to);
    update();
}

Spline* SplineChunk::spline() const
{
    return m_spline;
}

double SplineChunk::rangeFrom() const
{
    return m_range.first;
}

double SplineChunk::rangeTo() const
{
    return m_range.second;
}

QString SplineChunk::paintMode() const
{
    return m_paintMode;
}

void SplineChunk::setPaintMode(const QString &mode)
{
   m_paintMode = mode;
   emit paintModeChanged(mode);
   update();
}

double SplineChunk::maximumY() const
{
    return m_maximumY;
}

QSGNode *SplineChunk::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
    QSGGeometryNode *node = 0;
    QSGGeometry *geometry = 0;

    if (!oldNode) {
        node = new QSGGeometryNode();

        geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), m_segmentCount * 2);

        geometry->setLineWidth(2);
        geometry->setDrawingMode(
            m_paintMode == "line" ? GL_LINE_STRIP
                                  : GL_TRIANGLE_STRIP
            );

        node->setGeometry(geometry);
        node->setFlag(QSGNode::OwnsGeometry);

        QSGFlatColorMaterial *material = new QSGFlatColorMaterial;

        QColor color = m_spline->color();
        if (m_paintMode == "line") color.setAlphaF(1.0);
        material->setColor(color);

        node->setMaterial(material);
        node->setFlag(QSGNode::OwnsMaterial);

    } else {
        node = static_cast<QSGGeometryNode *>(oldNode);
        geometry = node->geometry();
        geometry->allocate(m_segmentCount * 2);
    }

    // It is easier to access the spline via ref
    const auto &spline = *m_spline;

    QRectF bounds = boundingRect();

    auto * const vertices = geometry->vertexDataAsPoint2D();

    const double step = (m_range.second - m_range.first) / (m_segmentCount - 1);

    // How much should we transform the spline to fit our bounds
    const double xScale = bounds.width() / (m_range.second - m_range.first);
    const double yScale = bounds.height() / spline.maximumY() / 2;
    const double yOffset = bounds.height() / 2;
    const double xOffset = - m_range.first * xScale;

    double maximumY = 0;
    double currentX = m_range.first;

    if (m_paintMode == "line") {
        for (int i = 0; i < m_segmentCount; ++i) {
            if (i == m_segmentCount - 1) {
                currentX = m_range.second;
            }

            const auto currentY = fmax(0, spline(currentX));

            vertices[i].set(
                xScale * currentX + xOffset,
                yScale * currentY + yOffset);

            vertices[2 * m_segmentCount - 1 - i].set(
                xScale * currentX + xOffset,
              - yScale * currentY + yOffset);

            if (maximumY < vertices[i].y) {
                maximumY = vertices[i].y;
            }

            currentX += step;
        }

    } else {
        for (int i = 0; i < m_segmentCount; ++i) {
            if (i == m_segmentCount - 1) {
                currentX = m_range.second;
            }

            const auto currentY = fmax(0, spline(currentX));

            vertices[2 * i].set(
                xScale * currentX + xOffset,
                yScale * currentY + yOffset);

            vertices[2 * i + 1].set(
                xScale * currentX + xOffset,
              - yScale * currentY + yOffset);

            if (maximumY < vertices[i].y) {
                maximumY = vertices[i].y;
            }

            currentX += step;
        }

    }

    m_maximumY = maximumY;
    qDebug() << "MaxY:" << maximumY;
    emit maximumYChanged(maximumY);

    // for (int i = 0; i < m_segmentCount; ++i) {
    //     qreal t = i / qreal(m_segmentCount - 1);
    //     qreal invt = 1 - t;
    //
    //     QPointF pos = invt * invt * invt * m_p1
    //                 + 3 * invt * invt * t * m_p2
    //                 + 3 * invt * t * t * m_p3
    //                 + t * t * t * m_p4;
    //
    //     float x = bounds.x() + pos.x() * bounds.width();
    //     float y = bounds.y() + pos.y() * bounds.height();
    //
    //     vertices[i].set(x, y);
    // }

    node->markDirty(QSGNode::DirtyGeometry);

    return node;

}
